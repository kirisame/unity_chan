﻿using UnityEngine;
using System.Collections;

public class EnemyShot : MonoBehaviour {
	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.position += transform.forward * 0.5f;
        Destroy(gameObject, 3.0f);
	}
    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.gameObject.name == "unitychan")
        {
        }
        else if (collisionInfo.gameObject.tag == "Attack")
        {
            Destroy(collisionInfo.gameObject);
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
