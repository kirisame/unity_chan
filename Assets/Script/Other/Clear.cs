﻿using UnityEngine;
using System.Collections;

public class Clear : MonoBehaviour {
    private AudioSource audioSource;
    public AudioClip WinSE;
	// Use this for initialization
	void Start () 
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(WinSE);
	}
	
	// Update is called once per frame
	void Update () 
    {

	}
}
