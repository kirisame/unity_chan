﻿using UnityEngine;
using System.Collections;

public class UI : MonoBehaviour {
    private Character_Action action;
    private int InstanceType = 0;
    private UISprite sprite;
	// Use this for initialization
	void Start () 
    {
        sprite = GameObject.Find("Attack_UI").GetComponent<UISprite>();
        action = GameObject.Find("unitychan").GetComponent<Character_Action>();

	}
	
	// Update is called once per frame
	void Update () 
    {
        InstanceType = action.AttackType();
        switch (InstanceType)
        {
            case 0:
                sprite.spriteName = "Wall";
                break;
            case 1:
                sprite.spriteName = "MoveCube";
                break;
            case 2:
                sprite.spriteName = "Sphere";
                break;
        }
	}

}
