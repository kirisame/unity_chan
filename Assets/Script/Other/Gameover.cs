﻿using UnityEngine;
using System.Collections;

public class Gameover : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip LoseSE;
    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(LoseSE);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
