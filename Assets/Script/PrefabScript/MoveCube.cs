﻿using UnityEngine;
using System.Collections;

public class MoveCube : MonoBehaviour {
    public GameObject Player;
	// Use this for initialization
	void Start ()
    {
        Player = GameObject.Find("unitychan");
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.position += transform.forward * 0.5f;
        Destroy(gameObject, 0.5f);
	}
}
