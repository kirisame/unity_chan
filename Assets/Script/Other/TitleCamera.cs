﻿using UnityEngine;
using System.Collections;

public class TitleCamera : MonoBehaviour {
    public GameObject Player;
    private float distance = -7.0f; //奥行き
    private float height = 1.0f; //高さ
	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        Quaternion rotation = Quaternion.Euler(0, 1, 0);
        Vector3 position = rotation * new Vector3(0.0f, height, distance) + Player.transform.position;
        transform.rotation = rotation;
        transform.position = position;

        transform.position = Quaternion.Euler(0, 1, 0) * new Vector3(0.0f, height, distance) + Player.transform.position;
	}
}
