﻿using UnityEngine;
using System.Collections;

public class ExplosionSphere : MonoBehaviour {
    public float ExPower = 50.0f; //爆発力
    public float ExRadius = 10.0f;//爆発の範囲
    public Vector3 MoveVec;

    private GameObject Player;

    private Rigidbody body;
	// Use this for initialization
	void Start () 
    {
        Player = GameObject.Find("unitychan");
        body = gameObject.collider.attachedRigidbody;
	}
	
	// Update is called once per frame
	void Update ()
    {
        MoveVec = transform.position - Player.transform.position;
        MoveVec = new Vector3(MoveVec.x,0,MoveVec.z);
        Vector3.Normalize(MoveVec);

        body.AddForce(MoveVec * 3.0f,ForceMode.Force);
        Invoke("Explosion", 3.0f);
	}

    void Explosion()
    {
        body.AddExplosionForce(ExPower, transform.position, ExRadius, 3.0f, ForceMode.Impulse);
    }
}
