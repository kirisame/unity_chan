﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
    private GameObject Player;
    private Vector3 PlayerLength;
    public Vector3 RotateVec;
    public Vector3 FloatVec = Vector3.zero;
    private Vector3 MoveVec = Vector3.zero;
    private int Count;
    private int MoveCount;

	// Use this for initialization
	void Start ()
    {
        Player = GameObject.Find("unitychan");
        MoveVec = new Vector3(Random.insideUnitSphere.x, 0, Random.insideUnitSphere.z);
	}
	
	// Update is called once per frame
	void Update ()
    {
        PlayerLength = Player.transform.position - transform.position; //プレイヤーとの距離
        Quaternion Qt = Quaternion.identity; //回転用のローカル変数
        RotateVec = Vector3.Normalize(PlayerLength);

        //プレイヤーと近いとき
        if (PlayerLength.sqrMagnitude <= 50.0f)
        {
            Count++;
            if (Count > 100)
            {
                Instantiate(Resources.Load("Prefab/EnemyBullet"), transform.position + transform.forward * 1.0f, transform.rotation);
                Count = 0;
            }
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(new Vector3(RotateVec.x, RotateVec.y + 0.2f, RotateVec.z)), 0.1f);
            //transform.position += RotateVec / 100.0f;
        }
        else
        {
            //離れているとき
            MoveCount++;
            if (MoveCount >= 50)
            {
                //ランダムに移動
                MoveVec = new Vector3(Random.insideUnitSphere.x, 0, Random.insideUnitSphere.z);
                MoveVec = Vector3.Normalize(MoveVec);
                MoveCount = 0;
            }
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(MoveVec), 0.1f);
            transform.position += MoveVec / 20.0f;
        }
        transform.position += FloatVec;

	}

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Attack")
        {
            Instantiate(Resources.Load("Prefab/DamageEffect"), transform.position, Quaternion.identity);
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }
}
