﻿using UnityEngine;
using System.Collections;

public class Character_Controller : MonoBehaviour {
    private Animator anim;
    private Vector3 MoveVec;
    public float Hp;
    public Terrain terrain;
    public GameObject Maincamera;
    private CharacterController controller;
    private UISprite HpSprite;

	// Use this for initialization
	void Start () 
    {
        anim = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        HpSprite = GameObject.Find("Gauge").GetComponent<UISprite>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        //コントローラーのスティックを倒した方向をとる
        float axisValueX = Input.GetAxis("Horizontal");
        float axisValueZ = Input.GetAxis("Vertical");
        //プレイヤーの移動ベクトルを制作
        MoveVec = (Maincamera.transform.right * Input.GetAxis("Horizontal")) + (Maincamera.transform.forward * Input.GetAxis("Vertical"));
        MoveVec.y = 0;
        //スティックを倒しているときに、倒した方向にプレイヤーが向くようにローテーションを設定
        if (System.Math.Abs(axisValueX) >= 0.1f || System.Math.Abs(axisValueZ) >= 0.1f)
        {
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Damage"))
            {
                if (Input.GetButton("Button_X"))
                {
                    controller.SimpleMove(MoveVec * 6.0f);
                }
                else
                {
                    controller.SimpleMove(MoveVec * 4.0f);
                }
                transform.rotation = Quaternion.LookRotation(MoveVec);
            }
        }
        //アニメーションのセット、マイナスの値だとアニメーションしないので絶対値で
        anim.SetFloat("Speed", System.Math.Abs(axisValueX) + System.Math.Abs(axisValueZ));

        if (HpSprite.fillAmount <= 0)
        {
            Application.LoadLevel("Gameover");
        }
	}

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "EnemyBullet")
        {
            HpSprite.fillAmount -= 0.1f;
            anim.SetTrigger("Damage");
            Destroy(collision.gameObject);
        }
    }
}
