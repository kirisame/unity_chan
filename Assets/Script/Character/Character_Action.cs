﻿using UnityEngine;
using System.Collections;

public class Character_Action : MonoBehaviour {
    private bool Jump = false; //ジャンプ中ならtrue
    public float Speed = 6.0f;
    public float jumpSpeed = 20.0f; //ジャンプする力
    public float Gravity = -9.8f;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;
    private Animator anim;
    private int Count = 0;
    private int InstanceType = 0;

    private AudioSource audioSource;
    public AudioClip JumpSE;
    public AudioClip AttackSE;

    private UISprite MpSprite;
	// Use this for initialization
	void Start () 
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        MpSprite = GameObject.Find("MPGauge").GetComponent<UISprite>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        float axisValueX = Input.GetAxis("Horizontal");
        float axisValueZ = Input.GetAxis("Vertical");
        if (System.Math.Abs(axisValueX) >= 0.1f || System.Math.Abs(axisValueZ) >= 0.1f)
        {
            jumpSpeed = 12.0f;
        }
        else
        {
            jumpSpeed = 8.0f;
        }

        //L,Rボタンでアクションをきえいかえる
        if (Input.GetButtonDown("Button_L"))
        {
            //最小値を見る
            if (InstanceType == 0)
            {
                InstanceType = 2;
            }
            else
            {
                InstanceType--;
            }
        }

        if (Input.GetButtonDown("Button_R"))
        {
            //最大値を見る
            if (InstanceType == 2)
            {
                InstanceType = 0;
            }
            else
            {
                InstanceType++;
            }
        }

        if(Input.GetButtonDown("Button_B"))
        {
            //MPが0以上のとき、攻撃、防御等できる
            if (MpSprite.fillAmount >= 0.1f)
            {
                switch (InstanceType)
                {
                    case 0:
                        Instantiate(Resources.Load("Prefab/Cube"), (transform.position + new Vector3(0, 1, 0)) + transform.forward * 1.0f, transform.rotation);
                        MpSprite.fillAmount -= 0.1f;
                        break;
                    case 1:
                        audioSource.PlayOneShot(AttackSE);
                        Instantiate(Resources.Load("Prefab/MoveCube"), (transform.position + new Vector3(0, 1, 0)) + transform.forward * 1.0f, transform.rotation);
                        MpSprite.fillAmount -= 0.1f;
                        break;
                    case 2:
                        Instantiate(Resources.Load("Prefab/ExplosionSphere"), (transform.position + new Vector3(0, 1, 0)), transform.rotation);
                        MpSprite.fillAmount -= 0.15f;
                        break;

                }
            }
            else
            {
                //MPが足りない
            }
            //Instantiate(Resources.Load("Prefab/MoveCube"), (transform.position + new Vector3(0,1,0)) + transform.forward * 1.0f, transform.rotation);
        }
        if (controller.isGrounded)
        {
            moveDirection = new Vector3(0, 0, 0);
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= Speed;
            if (!Jump && Input.GetButtonDown("Button_A") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Jump"))
            {
                Jump = true;
                anim.SetBool("Jump", true);
            }
            if (Jump)
            {
                    Count = 0;
                    audioSource.PlayOneShot(JumpSE);
                    moveDirection.y = jumpSpeed;
                    Jump = false;
            }
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Jump"))
            {
                anim.SetBool("Jump", false);
            }
        }
        //else
        //{
        //    anim.SetBool("Jump", false);
        //}

        moveDirection.y += Physics.gravity.y * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);

        //MPは常に少しずつ上昇
        MpSprite.fillAmount += 0.001f;
	}

    public int AttackType()
    {
        return InstanceType;
    }
}
